export default function canonize(url) {
  const re = new RegExp('@?(https?:)?(\/\/)?((www|telegram|vk|vkontakte|twitter|github|xn|medium)[^\/]*\/)?(@?[a-zA-Z0-9\._]*)', 'i');
  if (url == '') {
    return 'Invalid username';
  }
  const username = url.match(re)[5];
  if (username[0] == '@') {
    return username;
  } else {
    return '@' + username;
  }
}