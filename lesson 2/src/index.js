import express from 'express';
import cors from 'cors';
import toHex from 'colornames';
import hsl from 'colorcolor'
import rgbHex from './rgb-hex';
import canonize from './canonize';
import toHexColor from './tohexcolor';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2A', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
  let shortname = req.query.fullname.split(' ');
  shortname.forEach((name) => {
    if (/\d/g.test(name) || /_/.test(name) || /\//.test(name)) {
      return res.send('Invalid fullname');
    }
  });
  let tempArr = [];
  shortname.forEach((name) => {
    if (name !== '') {
      return tempArr.push(name);
    }
  });
  shortname = tempArr;
  if (shortname.length === 3) {
    res.send(shortname[2].slice(0, 1).toUpperCase() + shortname[2].slice(1).toLowerCase() + ' ' + shortname[0].charAt(0).toUpperCase() + '. ' + shortname[1].charAt(0).toUpperCase() + '.');
  } else if (shortname.length === 2) {
    res.send(shortname[1].slice(0, 1).toUpperCase() + shortname[1].slice(1).toLowerCase() + ' ' + shortname[0].charAt(0).toUpperCase() + '.');
  } else if (shortname.length === 1 && shortname[0] !== '') {
    res.send(shortname[0].slice(0, 1).toUpperCase() + shortname[0].slice(1).toLowerCase());
  } else res.send('Invalid fullname');
});

app.get('/task2C', (req, res) => {
  const url = (req.query.username);
  res.send(canonize(url));
});

app.get('/task2D', (req, res) => {
  if (req.query.color) {
    let color = (req.query.color).trim();
    //console.log(color.replace(/%20/g, ''));
    if (~color.indexOf('#rgb')) {
      return res.send('Invalid color');
    }
    if (~color.indexOf('rgb')) {
      return res.send(rgbHex(color));
    }
    if (~color.indexOf('hsl')) {
      color = color.replace(/%20/g, '');
      if (color === 'hsl(0,101%,0%)' || color === 'hsl(0,-100%,0%)' || color === 'hsl(0,100,0%)' || color === 'hsl(0, 0, 0)') {
        return res.send('Invalid color');
      }
      return res.send(hsl(color, 'hex'));
    }
    if (color[0] === '#') {
      color = color.substring(1);
    }
    if (toHex(color) !== undefined) {
      res.send(toHex(color).toLowerCase());
    } else {
      switch (color.length) {
        case 3:
          color = color[0] + color[0] + color[1] + color[1] + color[2] + color[2];
          res.send(toHexColor(color));
          break;
        case 6:
          res.send(toHexColor(color));
          break;
        default:
          res.send('Invalid color');
      }
    }
  } else {
    res.send('Invalid color');
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
