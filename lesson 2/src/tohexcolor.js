export default function toHexColor(color) {
  const hexColor = +('0x' + color);
  if (isNaN(hexColor)) {
    return 'Invalid color';
  } else if (hexColor >= 0 && hexColor <= 16777215) {
    return '#' + color.toLowerCase();
  }
  return 'Invalid color';
}
