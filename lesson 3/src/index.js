import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

function addRoute(path, value) {
  app.get(path, (req, res) => {
    res.status(200).json(value);
  });
}

function makeDynamicRoute(json, path) {
  path = path || '';
  if (typeof json === 'object') {
    for (const key in json) {
      if (json.hasOwnProperty(key)) {
        makeDynamicRoute(json[key], path + '/' + key);
      }
    }
  }
  console.log(path);
  addRoute(path, json);
}

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
    makeDynamicRoute(await pc, '/task3A/api');
    await app.get('/task3A/api/volumes', (req, res) => {
      let size1 = 0;
      let size2 = 0;
      pc.hdd.forEach((item) => {
        if (item.volume === 'C:') {
          size1 += item.size;
        } else { size2 += item.size; }
      });
      res.status(200).json({ 'C:': size1 + 'B', 'D:': size2 + 'B' });
    });
  })
   .then(async () => {
     await app.use((req, res) => {
       res.status(404).send('Not Found');
     });
   })
  .catch((err) => {
    console.log('Чтото пошло не так:', err);
  });

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
